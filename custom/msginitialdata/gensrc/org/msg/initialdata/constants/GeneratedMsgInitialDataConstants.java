/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 12, 2014 6:51:29 PM                     ---
 * ----------------------------------------------------------------
 */
package org.msg.initialdata.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedMsgInitialDataConstants
{
	public static final String EXTENSIONNAME = "msginitialdata";
	
	protected GeneratedMsgInitialDataConstants()
	{
		// private constructor
	}
	
	
}
