/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 12, 2014 6:51:29 PM                     ---
 * ----------------------------------------------------------------
 */
package org.msg.core.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedMsgCoreConstants
{
	public static final String EXTENSIONNAME = "msgcore";
	public static class TC
	{
		public static final String B2BCHECKOUTFLOWENUM = "B2BCheckoutFlowEnum".intern();
		public static final String B2BCHECKOUTPCIOPTIONENUM = "B2BCheckoutPciOptionEnum".intern();
		public static final String BTGORGANIZATIONTOTALSPENTINCURRENCYLASTYEAROPERAND = "BTGOrganizationTotalSpentInCurrencyLastYearOperand".intern();
		public static final String BTGORGANIZATIONTOTALSPENTINCURRENCYRELATIVEDATESOPERAND = "BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand".intern();
		public static final String MULTIPLECATALOGSSYNCCRONJOB = "MultipleCatalogsSyncCronJob".intern();
		public static final String ORGANIZATIONORDERSREPORTINGCRONJOB = "OrganizationOrdersReportingCronJob".intern();
		public static final String ORGANIZATIONORDERSTATISTICS = "OrganizationOrderStatistics".intern();
		public static final String POWERTOOLSSIZEVARIANTPRODUCT = "PowertoolsSizeVariantProduct".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	public static class Enumerations
	{
		public static class B2BCheckoutFlowEnum
		{
			public static final String MULTISTEP = "MULTISTEP".intern();
			public static final String SINGLE = "SINGLE".intern();
		}
		public static class B2BCheckoutPciOptionEnum
		{
			public static final String HOP = "HOP".intern();
			public static final String DEFAULT = "Default".intern();
		}
	}
	
	protected GeneratedMsgCoreConstants()
	{
		// private constructor
	}
	
	
}
